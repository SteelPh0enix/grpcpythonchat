import unittest

from chatserver.chatserver import *
from chatserver import chatroom


class TestChatServer(unittest.TestCase):

    def test_create_server(self):
        server = ChatServer()

    def test_add_room(self):
        test_room_name = "TestRoom"
        test_room_limited_name = "TestRoomLimited"
        test_room_wrong_limit_name = "TestRoomWrongLimit"

        server = ChatServer()

        self.assertEqual(server.add_room(test_room_name),
                         RoomCreationResult.SUCCESS)
        self.assertEqual(server.add_room(test_room_limited_name, 20), 
                         RoomCreationResult.SUCCESS)

        self.assertTrue(test_room_name in server.rooms)
        self.assertTrue(test_room_limited_name in server.rooms)
        self.assertEqual(server.rooms[test_room_limited_name].user_limit, 20)

        self.assertEqual(server.add_room(test_room_wrong_limit_name, -2),
                         RoomCreationResult.WRONG_LIMIT)
        self.assertFalse(test_room_wrong_limit_name in server.rooms)

        self.assertEqual(server.add_room(test_room_name),
                         RoomCreationResult.ROOM_EXISTS)

    def test_delete_room(self):
        test_room_name = "TestRoom"
        test_room_limited_name = "TestRoomLimited"
        test_room_not_existing_name = "NotExistingRoom"

        server = ChatServer()

        server.add_room(test_room_name)
        server.add_room(test_room_limited_name, 20)

        self.assertEqual(server.delete_room(test_room_name),
                         RoomDeletionResult.SUCCESS)
        self.assertEqual(server.delete_room(test_room_limited_name),
                         RoomDeletionResult.SUCCESS)
        self.assertEqual(server.delete_room(test_room_not_existing_name),
                         RoomDeletionResult.ROOM_NOT_EXISTS)

    def test_connect_user(self):
        test_user = {"username": "test_user"}
        test_user_second = {"username": "test_user_2"}

        server = ChatServer()

        self.assertEqual(server.connect_user(test_user),
                         UserConnectionResult.SUCCESS)
        self.assertEqual(server.connect_user(test_user_second),
                         UserConnectionResult.SUCCESS)
        self.assertEqual(server.connect_user(test_user),
                         UserConnectionResult.ALREADY_EXISTS)

    def test_add_user_to_chatroom(self):
        test_user_first = {"username": "test_user_1"}
        test_user_second = {"username": "test_user_2"}
        test_user_third = {"username": "test_user_3"}
        test_room_first = {"name": "room1"}
        test_room_second = {"name": "room2"}

        server = ChatServer()

        server.add_room(test_room_first)
        server.add_room(test_room_second)

        server.connect_user(test_user_first)
        server.connect_user(test_user_second)
        server.connect_user(test_user_third)

        self.assertEqual(server.add_user_to_chatroom(
                                server.get_user(test_user_first["username"]),
                                server.get_room(test_room_first["name"])),
                         chatroom.UserAdditionResult.SUCCESS)

        self.assertEqual(server.add_user_to_chatroom(
                                user_name_second, room_name_second),
                         chatroom.UserAdditionResult.SUCCESS)
        self.assertEqual(server.add_user_to_chatroom(
                                user_name_third, room_name_first),
                         chatroom.UserAdditionResult.SUCCESS)
        self.assertEqual(server.add_user_to_chatroom(
                                user_name_third, room_name_second),
                         chatroom.UserAdditionResult.SUCCESS)

        self.assertEqual(server.add_user_to_chatroom(
                                user_name_first, room_name_first),
                         chatroom.UserAdditionResult.USER_EXISTS)
        self.assertEqual(server.add_user_to_chatroom(
                                user_name_third, room_name_second),
                         chatroom.UserAdditionResult.USER_EXISTS)

    def test_disconnect_user(self):
        test_user = {"username": "test_user"}
        test_user_second = {"username": "test_user_2"}

        server = ChatServer()

        server.connect_user(test_user)
        server.connect_user(test_user_second)

        self.assertTrue(server.disconnect_user(
                        server.get_user(test_user["username"]),
                        UserDisconnectionReason.CLOSED_CONNECTION))
        self.assertTrue(server.disconnect_user(
                        server.get_user(test_user_second["username"]),
                        UserDisconnectionReason.QUIT))

    def test_remove_user_from_chatroom(self):
        pass

    def test_send_message(self):
        pass

    def test_reset(self):
        pass

if __name__ == '__main__':
    unittest.main()
