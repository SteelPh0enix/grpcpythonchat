from enum import Enum

from .user import *


class ChatroomUserLimitException(RuntimeError):
    pass


class UserAdditionResult(Enum):
    SUCCESS = 1
    ROOM_FULL = 2
    USER_EXISTS = 3
    ROOM_NOT_EXISTS = 4


class UserRemovalResult(Enum):
    SUCCESS = 1
    USER_NOT_EXISTS = 2
    ROOM_NOT_EXISTS = 3 


class Chatroom:
    def __init__(self, room_data):
        self.name = room_data["name"]
        user_limit = 0
        try:
            user_limit = room_data["user_limit"]
        except KeyError:
            pass

        self.user_limit = user_limit
        self.users = {}

    def send_message(self, sender, message_data):
        raise NotImplementedError

    def add_user(self, user):
        if self.user_limit != 0 and self.user_limit == len(self.users):
            return UserAdditionResult.ROOM_FULL

        if user.name in self.users:
            return UserAdditionResult.USER_EXISTS

        self.users[user.name] = user
        return UserAdditionResult.SUCCESS

    def remove_user(self, user, reason):
        raise NotImplementedError
