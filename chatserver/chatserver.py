from . import chatroom
from . import user
from enum import Enum


class RoomCreationResult(Enum):
    SUCCESS = 0
    ROOM_EXISTS = 1
    WRONG_LIMIT = 2


class RoomDeletionResult(Enum):
    SUCCESS = 0
    ROOM_NOT_EXISTS = 1


class UserConnectionResult(Enum):
    SUCCESS = 0
    ALREADY_EXISTS = 1


class UserDisconnectionReason(Enum):
    CLOSED_CONNECTION = 0
    QUIT = 1
    KICK = 2
    BAN = 3


class ChatServer:
    def __init__(self):
        self.rooms = {"Global": chatroom.Chatroom(room_name="Global")}
        self.users = {}

    def add_room(self, room_data):
        if name in self.rooms:
            return RoomCreationResult.ROOM_EXISTS

        try:
            self.rooms[name] = chatroom.Chatroom(room_data)
        except chatroom.ChatroomUserLimitException:
            return RoomCreationResult.WRONG_LIMIT

        return RoomCreationResult.SUCCESS

    def delete_room(self, name):
        if name not in self.rooms:
            return RoomDeletionResult.ROOM_NOT_EXISTS

        del self.rooms[name]
        return RoomDeletionResult.SUCCESS

    def add_user_to_chatroom(self, user, room):
        if room not in self.rooms:
            return chatroom.UserAdditionResult.ROOM_NOT_EXISTS

        return self.rooms[room].add_user(user)

    def remove_user_from_chatroom(self, user, reason):
        if room not in self.rooms:
            return chatroom.UserRemovalResult.ROOM_NOT_EXISTS

        return self.rooms[room].remove_user(user, reason)

    def connect_user(self, user_data):
        if user_data["username"] in self.users:
            return UserConnectionResult.ALREADY_EXISTS

        self.users[user_data["username"]] = user.User(user_data["username"])
        return UserConnectionResult.SUCCESS

    def disconnect_user(self, user_data, reason):
        raise NotImplementedError

    def send_message(self, sender, receiver, message_data):
        raise NotImplementedError

    def reset(self):
        raise NotImplementedError

    def get_user(self, username):
        return self.users[username]

    def get_room(self, room_name):
        return self.rooms[room_name]
